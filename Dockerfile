FROM centos:centos7

RUN yum install -y epel-release \
    && yum install -y nginx \
    && yum clean all \
    && echo "daemon off;" >> /etc/nginx/nginx.conf

#ONBUILD STUFF

ONBUILD RUN  rm -rf /etc/nginx/conf.d/*
ONBUILD COPY src/www.conf /etc/nginx/conf.d/www.conf
ONBUILD COPY src/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 443

CMD [ "/usr/sbin/nginx" ]
